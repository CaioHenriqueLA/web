﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace web
{
    public interface IRelatorio
    {
        Task Imprime(HttpContext context);
    }
}