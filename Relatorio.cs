﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web
{
    public class Relatorio : IRelatorio
    {
        private readonly ICatalogo _catalogo;

        public Relatorio(ICatalogo catalogo)
        {
            _catalogo = catalogo;
        }

        public async Task Imprime(HttpContext context)
        {
            foreach (var livro in _catalogo.GetLivros())
            {
                await context.Response.WriteAsync($"{livro.Codigo,-10}{livro.Nome,-40}{livro.Preco,10}\r\n");
            }
        }
    }
}
